
from project import db


db.Model.metadata.reflect(db.engine)


class User(db.Model):
    __table__ = db.Model.metadata.tables['user']

    # __table_args__ = {'extend_existing': True}
    # id = db.Column(db.Integer, primary_key=True)
    # first_name = db.Column(db.String(35), nullable=False)
    # last_name = db.Column(db.String(35), nullable=False)

    def __init__(self, user_first_name, user_last_name):
        self.user_first_name = user_first_name
        self.user_last_name = user_last_name

    def __repr__(self):
        return '<User %r>' % self.username


