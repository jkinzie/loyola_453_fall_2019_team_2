

from project.models import User
from project import db

from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed
from wtforms import StringField, PasswordField, SubmitField, BooleanField, TextAreaField, IntegerField, DateField, SelectField, HiddenField
from wtforms.validators import DataRequired, Length, Email, EqualTo, ValidationError,Regexp
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from wtforms.fields.html5 import DateField


class NewUserForm(FlaskForm):

    user_first_name = StringField('First Name',
                           validators=[DataRequired(), Length(min=2, max=20)])

    user_last_name = StringField('Last Name',
                           validators=[DataRequired(), Length(min=2, max=20)])

    submit = SubmitField('Add User')



    
class DeleteUserForm(FlaskForm):

    user_first_name = StringField('First Name',
                           validators=[DataRequired(), Length(min=2, max=20)])

    user_last_name = StringField('Last Name',
                           validators=[DataRequired(), Length(min=2, max=20)])

    submit = SubmitField('Add User')