from flask import render_template, url_for, flash, redirect, request, abort
from project import app, db
from project.models import User
from project.forms import NewUserForm


@app.route("/")
@app.route("/home")
def home():
    users = User.query.all()
    return render_template('home.html', users=users)


@app.route("/user/new", methods=['GET', 'POST'])
def new_user():
    form = NewUserForm()
    if form.validate_on_submit():
        user = User(user_first_name=form.user_first_name, user_last_name=form.user_last_name)

    return redirect(url_for('home'))
