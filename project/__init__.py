from flask import Flask
from flask_sqlalchemy import *

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://ike:ikecomp453@45.55.59.121/ike'
db = SQLAlchemy(app)

from project import routes
from project import models


models.db.create_all()



